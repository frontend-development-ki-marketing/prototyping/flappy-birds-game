// Der Hauptkonzept dieses Codes basiert auf dem Flappy Bird Spiel von:
// https://github.com/kaizhelam/Flappy-Bird-Games

// Selektiere die HTML-Elemente für den Vogel und das Bild
let bird = document.querySelector('.bird');
let img = document.getElementById('bird-1');

// Hole die Eigenschaften des Vogel-Elements und des Hintergrund-Elements
let bird_props = bird.getBoundingClientRect();
let background = document.querySelector('.background').getBoundingClientRect();

// Selektiere die Elemente für den Punktestand, die Nachricht und den Titel
let score_val = document.querySelector('.score_val');
let message = document.querySelector('.GameOverMessage');
let score_title = document.querySelector('.score_title');

// Initialisiere Variablen für die Bewegung und Physik des Vogels
let bird_dy = 0;
let gravity = 0.05;
let move_speed = 3.5;
let iPipeseperation = 130;
let jumpheight = -5.6;

// Initialisiere Variablen für die Cursorposition und andere Zustände
let realX = 1;
let realY = 1;
let oldY = 0;
let birdflag = false;

// Initialisiere Variablen für die Sprachsynthese
const mascot = document.getElementById("mascot");
const speechBubble = document.getElementById("speechBubble");
let speaking = false;
let mouthInterval;
let textIndex = 0;
let currentText = '';
let showNextLetterTimeout;

// Erstelle eine WebSocket-Verbindung zum Server
const ws = new WebSocket("ws://localhost:453/ws");

// Initialisiere den Cursor und andere Zustandsvariablen
let cursor = document.getElementById("cursor");
let flag = false;
let click = null;

// WebSocket-Nachrichtenevent: Aktualisiere Cursorposition und simuliere Klicks
ws.onmessage = function (event){
    const data = JSON.parse(event.data);
    realY = window.innerHeight * data.y;
    realX = window.innerWidth * data.x;
    click = data.click;

    cursor.style.left = (realX) + "px";
    cursor.style.top = (realY) + "px";
    if (click && !flag){
        const Element = document.elementFromPoint(realX, realY);
        if (Element) {
            simulateClick(Element);
        }
    }
};

// Setze den Spielzustand auf 'Start' und verstecke das Vogelbild
let game_state = 'Start';
img.style.display = 'none';
message.classList.add('messageStyle');

// Funktion zum Abrufen von Sprachsynthese-Stimmen
function setSpeech() {
    return new Promise(
        function (resolve, reject) {
            let synth = window.speechSynthesis;
            let id;

            id = setInterval(() => {
                if (synth.getVoices().length !== 0) {
                    resolve(synth.getVoices());
                    clearInterval(id);
                }
            }, 10);
        }
    )
}

// Funktion zum Sprechen eines Textes
function speak(text) {
    speaking = true;
    currentText = text;
    textIndex = 0;
    const speechBubble = document.getElementById("speechBubble");
    speechBubble.textContent = '';
    clearInterval(mouthInterval);
    mouthInterval = setInterval(toggleMouth, 500);
    showNextLetterTimeout = setTimeout(showNextLetter, 50);
}

// Funktion zum Anzeigen des nächsten Buchstabens in der Sprechblase
function showNextLetter() {
    const speechBubble = document.getElementById("speechBubble");
    if (textIndex < currentText.length) {
        speechBubble.textContent += currentText.charAt(textIndex);
        textIndex++;
        showNextLetterTimeout = setTimeout(showNextLetter, 50);
    } else {
        speaking = false;
        clearInterval(mouthInterval);
        resetMouth();
    }
}

// Funktion zum Umschalten des Mundzustands des Maskottchens
function toggleMouth() {
    mascot.classList.toggle('mouth-open');
    mascot.classList.toggle('mouth-closed');
}

// Funktion zum Zurücksetzen des Mundzustands des Maskottchens
function resetMouth() {
    mascot.classList.remove('mouth-open');
    mascot.classList.remove('mouth-closed');
}

// Event-Listener für Klicks auf das Maskottchen
document.getElementById("mascot").addEventListener("click", function() {
    if (!speaking) {
        speechSynthesis.cancel();
        speak("Willkommen zu Flappy Birds! Deine Hand wird als Cursor erkannt, nutze diese um auf Start zu drücken. Weiche den Hindernissen aus indem du mit deiner Hand den Vogel springen lässt. Pass auf, falls der Vogel sich den Kopf stößt oder gegen eine der Röhren fliegt, ist es vorbei!");
        let s = setSpeech();
        let speech = new SpeechSynthesisUtterance();
        s.then((voices) => {
            speech.voice = voices.find(a => a.lang === "de-DE");
            speech.text = currentText;
            speech.lang = "de-DE";
            speech.volume = 1;
            speech.voiceURI = "native";
            speechSynthesis.cancel();
            window.speechSynthesis.speak(speech);
            speechBubble.style.display = "block";
        });
    } else {
        speechSynthesis.cancel();
        speaking = false;
        clearInterval(mouthInterval);
        resetMouth();
        speechBubble.style.display = 'none';
        clearTimeout(showNextLetterTimeout);
    }
});

// DOMContentLoaded-Event: Initialisiere Event-Listener und Funktionen
document.addEventListener('DOMContentLoaded', () => {
    const startButton = document.getElementById('start-button');
    startButton.addEventListener('click', () => {
        if (game_state !== 'Play') {
            message.innerHTML = '';
            score_title.innerHTML = 'Score : ';
            score_val.innerHTML = '0';
            message.classList.remove('messageStyle');
            startButton.style.display = 'none';
            play();
        }
    });

    const infoButton = document.getElementById('infoButton');
    const infoBox = document.getElementById('infoBox');
    const closeInfoBox = document.getElementById('closeInfoBox');

    infoButton.addEventListener('click', () => {
        infoBox.style.display = 'block';
    });

    closeInfoBox.addEventListener('click', () => {
        infoBox.style.display = 'none';
    });

    window.addEventListener('click', (event) => {
        if (event.target === infoBox) {
            infoBox.style.display = 'none';
        }
    });
});

// Funktion zur Simulation eines Klicks auf ein Element
function simulateClick(element) {
    if (element) {
        element.click();
    }
}

// Funktion zum Hinzufügen des Neustart-Buttons
function setRestartButton(){
    var container = document.getElementById("gameover");
    const restartButton = document.createElement('button');
    restartButton.textContent = "Play again!";
    restartButton.id = "restartButton";
    restartButton.classList.add("start-button");
    restartButton.addEventListener('click', play);
    container.appendChild(restartButton);
}

// Hauptspiellogik
function play() {
    game_state = 'Play';
    document.querySelectorAll('.pipe_sprite').forEach((e) => {
        e.remove();
    });
    img.style.display = 'block';
    bird.style.top = '40vh';

    // Setze ein Intervall, um die Geschwindigkeit des Vogels im Laufe der Zeit zu erhöhen
    let speedIncreaseInterval = setInterval(() => {
        if (game_state === 'Play') {
            move_speed += 0.1; // Erhöhe die Geschwindigkeit um 0.1
            gravity += 0.001; // Erhöhe die Gravitation leicht
        } else {
            clearInterval(speedIncreaseInterval); // Lösche das Intervall, wenn das Spiel nicht im Zustand 'Play' ist
        }
    }, 5000); // Erhöhe die Geschwindigkeit alle 5 Sekunden

    requestAnimationFrame(create_pipe);

    // Funktion zur Bewegung der Rohre und Kollisionserkennung
    function move() {
        if (game_state !== 'Play') {
            return;
        }
        let pipe_sprite = document.querySelectorAll('.pipe_sprite');
        pipe_sprite.forEach((element) => {
            let pipe_sprite_props = element.getBoundingClientRect();
            bird_props = bird.getBoundingClientRect();

            if (pipe_sprite_props.right <= 0) {
                element.remove();
            } else {
                if (bird_props.left < pipe_sprite_props.left + pipe_sprite_props.width &&
                    bird_props.left + bird_props.width > pipe_sprite_props.left &&
                    bird_props.top < pipe_sprite_props.top + pipe_sprite_props.height &&
                    bird_props.top + bird_props.height > pipe_sprite_props.top) {
                    game_state = 'End';
                    document.querySelector('.GameOverMessage').innerHTML = 'Game Over'.fontcolor('red');
                    message.classList.add('GameOverMessageStyle');
                    img.style.display = 'none';
                    setRestartButton();
                } else {
                    if (pipe_sprite_props.right < bird_props.left &&
                        pipe_sprite_props.right + move_speed >= bird_props.left &&
                        element.increase_score == '1') {
                        score_val.innerHTML = +score_val.innerHTML + 1;
                    }
                    element.style.left = pipe_sprite_props.left - move_speed + 'px';
                }
            }
        });
        requestAnimationFrame(move);
    }
    requestAnimationFrame(move);

    // Funktion zur Anwendung der Gravitation
    function apply_gravity() {
        if (game_state != 'Play') return;
        bird_dy += gravity;

        if ((realY + 50) < oldY && !birdflag) {
            birdflag = true;
            bird_dy = jumpheight;
            setTimeout(() => {
                img.src = 'images/Bird.png';
            }, "300");
            img.src = 'images/Bird-2.png';
            setTimeout(() => {
                birdflag = false;
            }, "400");
        }

        oldY = realY;

        if (bird_props.top <= 0 || bird_props.bottom >= background.bottom) {
            game_state = 'End';
            message.style.left = '28vw';
            window.location.reload();
            message.classList.remove('GameOverMessageStyle');
            setRestartButton();
            return;
        }
        bird.style.top = `${parseFloat(bird.style.top) + bird_dy}px`;
        bird_props = bird.getBoundingClientRect();
        requestAnimationFrame(apply_gravity);
    }
    requestAnimationFrame(apply_gravity);

    // Funktion zur Erstellung der Rohre
    let pipe_separation = 0;
    let pipe_gap = 35;

    function create_pipe() {
        if (game_state !== 'Play') return;

        if (pipe_separation > iPipeseperation) {
            pipe_separation = 0;
            let pipe_posi = Math.floor(Math.random() * 43) + 8;
            let pipe_sprite_inv = document.createElement('div');
            pipe_sprite_inv.className = 'pipe_sprite';
            pipe_sprite_inv.style.top = pipe_posi - 70 + 'vh';
            pipe_sprite_inv.style.left = '100vw';

            document.body.appendChild(pipe_sprite_inv);

            let pipe_sprite = document.createElement('div');
            pipe_sprite.className = 'pipe_sprite';
            pipe_sprite.style.top = pipe_posi + pipe_gap + 'vh';
            pipe_sprite.style.left = '100vw';
            pipe_sprite.increase_score = '1';

            document.body.appendChild(pipe_sprite);
        }
        pipe_separation++;
        requestAnimationFrame(create_pipe);
    }
}
